gem 'minitest', '~> 5.4'
require 'minitest/autorun'
require_relative '../../../lib/core_extensions/array_functions/flattener'
Array.include CoreExtensions::ArrayFunctions::Nutshell

class FlattenerTest < Minitest::Test
  def setup
    @custom = Array.new

    @simple_array = [1,2,3,4]

    @simple_nest  = [1,2,[3],4]
    @complex_nest = [1,[[2],3],4]

    @edge_first   = [[[1]],[2,3],4]
    @edge_last    = [1,2,[[3]],[[[4]]]]

    @bad_input_1  = 42
    @bad_input_2  = {foo: :bar, woo: :tang}

    @mixed_array_1  = [1,2,"buckle",:shoe]
  end

  def test_raises_error_on_non_array_input_one
    assert_raises RuntimeError do
      @custom.flattener(@bad_input_1)
    end
  end

  def test_raises_error_on_non_array_input_two
    assert_raises RuntimeError do
      @custom.flattener(@bad_input_2)
    end
  end

  def test_simple_array_returned_on_simple_input
    assert_equal @simple_array, @custom.flattener(@simple_array)
  end

  def test_simple_array_returned_on_simple_nest_input
    assert_equal @simple_array, @custom.flattener(@simple_nest)
  end

  def test_simple_array_returned_on_complex_nest_input
    assert_equal @simple_array, @custom.flattener(@complex_nest)
  end

  def test_simple_array_returned_on_first_edge_case_input
    assert_equal @simple_array, @custom.flattener(@edge_first)
  end

  def test_simple_array_returned_on_last_edge_case_input
    assert_equal @simple_array, @custom.flattener(@edge_last)
  end

  def test_mixed_array_returned_on_mixed_array_input
    assert_equal @mixed_array_1, @custom.flattener(@mixed_array_1)
  end
end
