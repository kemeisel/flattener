module CoreExtensions
  module ArrayFunctions
    module Nutshell
      def flattener(arbitrary_array)
        raise RuntimeError unless arbitrary_array.class == Array

        return_array = []

        arbitrary_array.each do |element|
          if element.class == Array
            return_array.concat Array.new.flattener(element)
          else
            return_array.concat [element]
          end
        end

        return_array
      end
    end
  end
end