flattener
====================================

Flattener is a Ruby Array monkey-patch that accepts an arbitrarily deep array and returns the same flattened to a single dimension.

For additional information, see the [original specification][specification].


Getting Started
------------------------------------

### Prerequisites
You'll need to install [git][git] to clone this repository, ruby-bundler to build the application, and [Ruby][Ruby] to run the code.
```
sudo apt install git ruby ruby-bundler
```

### Installing

Installation is as simple as cloning the repository...

```
git clone https://kemeisel@bitbucket.org/kemeisel/flattener.git
cd flattener
```

...and bundling it to install any missing gems:
```
bundle install
```

Running the tests
------------------------------------
There is a Guardfile that runs pertinent tests on launch and whenever they (or the source code they watch) are saved.  To enable this functionality:
```
bundle exec guard
```

**NOTE**: You must be in the root of the flattener repository for this command to function correctly.


Next Steps
------------------------------------
Implemented as part of a larger project, there would likely be a configuration option to automatically load the contents of the `/lib`, obviating the manually-inserted `Array.include CoreExtensions::ArrayFunctions::Nutshell` found in the test file.

I'm sure there's also a more robust way to target the watched source file.  Though I've done TDD (using RSpec), I'm playing around here with Minitest for (essentially) the first time...

[specification]: https://github.com/nutshellcrm/join-the-team/blob/master/developer-questions.md#flattener
[git]: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
[Ruby]: https://www.ruby-lang.org/en/documentation/installation
